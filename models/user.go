package models

import (
	"fmt"
	"github/epith/302-backend-user/bob"
	"net/http"
)

type UserRequest struct {
	*bob.User
}

func (user *UserRequest) Bind(r *http.Request) error {
	if user.Username == "" {
		return fmt.Errorf("username is a required field")
	}
	if user.Passwordhash == "" {
		return fmt.Errorf("password is a required field")
	}

	return nil
}

type UserUpdateRequest struct {
	*bob.User
}

func (user *UserUpdateRequest) Bind(r *http.Request) error {
	if user.User == nil {
		return fmt.Errorf("Missing required User fields")
	}
	return nil
}
